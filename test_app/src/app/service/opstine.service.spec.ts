import { TestBed } from '@angular/core/testing';

import { OpstineService } from './opstine.service';

describe('OpstineService', () => {
  let service: OpstineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpstineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
