import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IOpstina } from '../model/opstina';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OpstineService {  
  
  constructor(private http: HttpClient) { 
  }

  getOpstineLista(): Observable<IOpstina[]> {
    var host = '';
    if (environment.apiUrl != null) {
      console.info('Environment api url found on' + environment.apiUrl);
      host = environment.apiUrl;
    } else {
      var myContext = window.location.pathname.substring(0, window.location.pathname.lastIndexOf("test_app/browser"));
      host = window.location.origin + myContext;
      console.info(`Context path created : ${myContext} extracted from ` + window.location.pathname);
    }
    var fullPath = host + 'backend/erp/opstine';
    console.info(`Full path is : ${fullPath}`);
    return this.http.get<IOpstina[]>(`${fullPath}`);
  }

}
