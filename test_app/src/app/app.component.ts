import { Component, inject, OnInit, viewChild, ViewChild } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { OpstineService } from './service/opstine.service.js';
import { IOpstina } from './model/opstina.js';
import { CommonModule } from '@angular/common';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CommonModule, MatTableModule, MatPaginatorModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit{

  title = 'opstine';
  opstineList: IOpstina[] = []
  opstineService = inject(OpstineService);

  displayedColumns: string[] = ['id', 'ptt', 'naziv'];
  dataSource = new MatTableDataSource<IOpstina>(this.opstineList);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngOnInit(): void {
    this.loadAllTask();
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  loadAllTask() {
    this.opstineService.getOpstineLista().subscribe(
      (res: IOpstina[]) => {
      this.opstineList = res;
      console.log(res)
      this.dataSource.data = res;
      },
      (error: Error) => {
        console.error('Error fetching data', error.message)
      })
  }

}
