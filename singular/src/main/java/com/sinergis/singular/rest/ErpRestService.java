package com.sinergis.singular.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/erp")
public class ErpRestService {

	private static final String COMMA_DELIMITER = ",";

	@GET
    @Path("/info")
    @Produces(MediaType.APPLICATION_JSON)
	@PermitAll
    public HashMap<String, String> getInfo() {
		HashMap<String, String> mapa = new HashMap<>();
		mapa.put("Status", "ACTIVE");
		mapa.put("Version", "1.0");
		return mapa;				
	}

	@GET
    @Path("/opstine")
    @Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public List<OpstinaDto> opstine(@Context HttpServletRequest request) throws Exception {
		List<OpstinaDto> records = new ArrayList<>();
		File file = new File(getClass().getClassLoader().getResource("opstine.csv").toURI());
		System.out.println(file.getAbsolutePath());
		FileReader in = new FileReader(file);
		try (BufferedReader br = new BufferedReader(in)) {
			br.readLine();
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(COMMA_DELIMITER);
		        records.add(new OpstinaDto(Long.valueOf(values[0]),values[1],values[2]));
		    }
		}
		return records;
	}

}
