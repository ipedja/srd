package com.sinergis.singular.rest;

public class OpstinaDto {
	public OpstinaDto(Long id, String ptt, String naziv) {
		super();
		this.id = id;
		this.ptt = ptt;
		this.naziv = naziv;
	}
	private Long id;
	private String ptt;
	private String naziv;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPtt() {
		return ptt;
	}
	public void setPtt(String ptt) {
		this.ptt = ptt;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
}
