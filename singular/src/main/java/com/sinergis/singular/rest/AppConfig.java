package com.sinergis.singular.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;

@ApplicationPath("/backend")
public class AppConfig extends ResourceConfig {
	
	public AppConfig() 
    {
		register(CORSFilter.class);
        register(ErpRestService.class);

		register(MultiPartFeature.class);
        EncodingFilter.enableFor(this, GZipEncoder.class);
        //Register Auth Filter here
    }
}