package com.sinergis.singular;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("mytheme")
@Title("Singular - hello world")
public class HelloWorldUI extends UI {
	@Override
	protected void init(VaadinRequest request) {
		// Create the content root layout for the UI
		TextField field = new TextField("Unesi");
		VerticalLayout content = new VerticalLayout(field);
		setContent(content);

		// Display the greeting
		content.addComponent(new Label("Hello World!"));

		// Have a clickable button
		content.addComponent(new Button("Push Me!", click -> Notification.show("Zdravo " + field.getValue(), Type.TRAY_NOTIFICATION)));
	}

	@WebServlet(urlPatterns = {"/ui/*","/VAADIN/*"}, name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = HelloWorldUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}
	
	
}